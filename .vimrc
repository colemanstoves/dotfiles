"   Tab Control
set tabstop=4
set softtabstop=4
set expandtab

"   Line Numbers
set number relativenumber

"   Word Wrap
set wrap
set linebreak

"   Syntax Highlighting
syntax on

"   Status Bar
set laststatus=1
set statusline=%filename

"   Color Schemes
"colo slate
"colo elflord
"colo murphy
"colo desert
"   VERY BRIGHT SCHEMES
"colo morning
