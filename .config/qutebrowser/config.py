config.load_autoconfig(False)
c.url.default_page = '~/localWebpage/index.html'
c.url.start_pages = '~/localWebpage/index.html'
c.downloads.location.directory = '~/Downloads'
#c.content.blocking.adblock.lists = ['https://abp.oisd.nl/', 'https://www.i-dont-care-about-cookies.eu/abp/'] 
c.content.blocking.adblock.lists = ['https://easylist.to/easylist/easylist.txt', 'https://easylist.to/easylist/easyprivacy.txt', 'https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt', 'https://www.i-dont-care-about-cookies.eu/abp/', 'https://secure.fanboy.co.nz/fanboy-cookiemonster.txt']
c.content.blocking.method = 'both'
config.bind(',m', 'hint links spawn mpv {hint-url}')
c.url.searchengines = {"DEFAULT": "https://engine.presearch.org/search?q={}"}
c.fonts.default_size = '10pt'
c.tabs.favicons.scale = 1.5
c.tabs.indicator.padding = {"bottom":2,"left":0,"right":0,"top":3}
c.tabs.padding = {"bottom":1,"left":1,"right":0,"top":0}
c.auto_save.session = True
c.spellcheck.languages = ["en-US"]
#c.statusbar.widgets = ["keypress", "search_match", "url", "scroll", "history", "tabs", "progress"]
c.statusbar.widgets = ["keypress", "search_match", "url", "scroll", "tabs", "progress", "clock"]
