#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# font coloring
PROMPTCOL="$(tput setaf 11)"
RESET="$(tput sgr0)"

# aliases are stored in the .bash_aliases file
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Prompt Coloring #
#PS1='${PROMPTCOL}[\u@\h \W]\$ ${RESET}'
PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"

# nnn File Explorer #
export NNN_PLUG='p:preview-tui;i:imgview'
export NNN_FIFO=/tmp/nnn.fifo
